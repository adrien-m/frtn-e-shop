<?php
/**
 * Template Name: ABOUT
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package understrap
 */

get_header('home');
// $container = get_theme_mod( 'understrap_container_type' );
?>

<div id="barba-wrapper">
<div class="barba-container">

<div class="container <?php body_class(); ?>">

	<div class="<?php echo esc_html( $container ); ?>" id="content">

		<div class="row">

<!-- 			<div class="container" id="primary"> -->

				<main class="site-main" id="main" role="main">

					<div class="row container">

						<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">

							<h3 class="text-about">Hello and welcome, you enter the Fortune Paris kingdom e-shop.
								Here you can find our inspiration and ideas that we develop.
								Most of it are clothings, but we don't want to be fix to one discipline,
								we like to try, test and have fun with to do this. Our clothing pieces are everyday
								outfit and easy to wear. 
							</h3>
						</div>

					</div>
					<div class="row container">

						<div class="col-xs-12 col-sm-12 offset-md-8 col-md-4 col-lg-4 offset-lg-8 col-xl-4">

							<h3 class="text-about">Hello and welcome, you enter the Fortune Paris kingdom e-shop.
								Here you can find our inspiration and ideas that we develop.
								Most of it are clothings, but we don't want to be fix to one discipline,
								we like to try, test and have fun with to do this. Our clothing pieces are everyday
								outfit and easy to wear. 
							</h3>
						</div>

					</div>
									


				</main><!-- #main -->

			<!-- </div>#primary -->

		</div><!-- .row end -->

	</div><!-- Container end -->

</div><!-- Wrapper end -->

</div>
</div>

<?php get_footer(); ?>
