<?php
/**
 * Template Name: Home
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package understrap
 */

get_header('home');
?>

<div id="barba-wrapper">
	<div class="barba-container">

		<div id="main" class="<?php body_class(); ?>">

			<div class="row">

				<div class="<?php echo esc_html( $container ); ?>" id="content">

					<main class="site-main scene_element scene_element--fadein" id="main" role="main">

						<div class="container-fluid padding-top-5">
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
									<h1 class="big-title">Nouvelle collection available
										<a href="#">See the collection</a>
									</h1>
								</div>
								<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
									<img src="<?php the_field('big_photo_one'); ?>" alt="" class="big_picture">
								</div>
							</div>
						</div>
								
						<div class="container-fluid padding-top-5 padding-bottom-5">
							<h3 class="best">Best sellers</h3>
							<div class="row">

								<div class="product product--1 col-md-4 col-md-offset-1 col-lg-4">
									<?php
							        	$args = array( 'post_type' => 'product', 'posts_per_page' => 1, 'product_cat' => 'one', 'orderby' => 'rand' );
								        $loop = new WP_Query( $args );
								        while ( $loop->have_posts() ) : $loop->the_post(); global $product; ?>  

						                    <a href="<?php echo get_permalink( $loop->post->ID ) ?>" title="<?php echo esc_attr($loop->post->post_title ? $loop->post->post_title : $loop->post->ID); ?>">

					                        <?php if (has_post_thumbnail( $loop->post->ID )) echo get_the_post_thumbnail($loop->post->ID, 'shop_catalog'); else echo '<img src="'.woocommerce_placeholder_img_src().'" alt="Placeholder" width="300px" height="300px" />'; ?>

						                        <h3 class="title__product"><?php the_title(); ?></h3>

						                        <span class="price"><?php echo $product->get_price_html(); ?></span>                    

						                    </a>

						                    <?php woocommerce_template_loop_add_to_cart( $loop->post, $product ); ?>

							    		<?php endwhile; ?>
								    <?php wp_reset_query(); ?>
								</div>

								<div class="product product--2 col-md-4 col-lg-4">
									<?php
								        $args = array( 'post_type' => 'product', 'posts_per_page' => 1, 'product_cat' => 'two', 'orderby' => 'rand' );
								        $loop = new WP_Query( $args );
								        while ( $loop->have_posts() ) : $loop->the_post(); global $product; ?>  

						                    <a href="<?php echo get_permalink( $loop->post->ID ) ?>" title="<?php echo esc_attr($loop->post->post_title ? $loop->post->post_title : $loop->post->ID); ?>">

					                        <?php if (has_post_thumbnail( $loop->post->ID )) echo get_the_post_thumbnail($loop->post->ID, 'shop_catalog'); else echo '<img src="'.woocommerce_placeholder_img_src().'" alt="Placeholder" width="300px" height="300px" />'; ?>

						                        <h3 class="title__product"><?php the_title(); ?></h3>

						                        <span class="price"><?php echo $product->get_price_html(); ?></span>                    

						                    </a>

						                    <?php woocommerce_template_loop_add_to_cart( $loop->post, $product ); ?>

								    	<?php endwhile; ?>
								    <?php wp_reset_query(); ?>
								</div>

								<div class="product product--3 col-md-4 col-lg-4">
									<?php
								        $args = array( 'post_type' => 'product', 'posts_per_page' => 1, 'product_cat' => 'three', 'orderby' => 'rand' );
								        $loop = new WP_Query( $args );
								        while ( $loop->have_posts() ) : $loop->the_post(); global $product; ?>  

						                    <a href="<?php echo get_permalink( $loop->post->ID ) ?>" title="<?php echo esc_attr($loop->post->post_title ? $loop->post->post_title : $loop->post->ID); ?>">

					                        <?php if (has_post_thumbnail( $loop->post->ID )) echo get_the_post_thumbnail($loop->post->ID, 'shop_catalog'); else echo '<img src="'.woocommerce_placeholder_img_src().'" alt="Placeholder" width="300px" height="300px" />'; ?>

						                        <h3 class="title__product"><?php the_title(); ?></h3>

						                        <span class="price"><?php echo $product->get_price_html(); ?></span>                    

						                    </a>

						                    <?php woocommerce_template_loop_add_to_cart( $loop->post, $product ); ?>

									    <?php endwhile; ?>
								    <?php wp_reset_query(); ?>
								</div>
							</div>
						</div>

						<div class="container-fluid">
							<div class="row justify-content-center">
    							<div class="col-4 green-block green-block-js">
    								<h3 class="newsletter">Newsletter here</h3>
    							</div>
    							<div class="col-4 yellow-block yellow-block-js">
    								<h3 class="insta">Instagram</h3>
    								<ul>
										<li class="button" id="time"><p id="hours"></p><p id="min"><p id="sec"></p></p></li>
									  	<li class="button" id="city"></li>
									  	<li class="button" id="weatherType"></li>
									  	<li class="button" id="fTemp"></li>
									  	<!-- <li class="button" id="windSpeed"></li> -->
									</ul>
    							</div>
    						</div>
    					</div>

					</main><!-- #main -->

				</div><!-- Container end -->

			</div>

		</div>

	</div>
</div>



<?php get_footer(); ?>

<!-- 	</div>
</div> -->
