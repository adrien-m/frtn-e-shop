<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package understrap
 */

$the_theme = wp_get_theme();
$container = get_theme_mod( 'understrap_container_type' );
?>

<?php get_sidebar( 'footerfull' ); ?>

<!-- <div class="wrapper" id="wrapper-footer"> -->

<!-- 	<div class="<?php echo esc_html( $container ); ?>">

		<div class="row"> -->

			<!-- <div class="col-md-12"> -->

				<footer class="site-footer" id="colophon">

					 <div class="marquee3k" data-speed="60" >
						<?php the_field('marquee_text'); ?> <br> 
					</div> 
					
					<?php
						wp_nav_menu( array(
							'theme_location' => 'third-menu',
					    	'container_class' => 'nav__container3',
							'container_id'    => 'nav__container3',
							'menu_class'      => 'navbar-nav3',
							'menu_id'			=> 'nav__link3',
							'fallback_cb'     => '',
							'menu_id'         => 'nav__link3',
							'walker'          => new WP_Bootstrap_Navwalker() )
					    );
					?>


				</footer><!-- #colophon -->

			<!-- </div>col end  -->

		<!-- </div>row end -->

	<!-- </div>container end -->

<!-- </div>wrapper end -->

</div><!-- #page -->





<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.19.1/jquery.gsap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.19.1/TweenMax.min.js"></script>
<script src="https:///cdn.jsdelivr.net/jquery.marquee/1.3.1/jquery.marquee.min.js"></script>
<script src="http://www.fortune-paris.com/wp-content/themes/understrap/js/marquee3k.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/barba.js/1.0.0/barba.min.js"></script>



<?php wp_footer(); ?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/smoothState.js/0.7.2/jquery.smoothState.min.js"></script>


<!-- </div>
</div> -->



</body>

</html>
